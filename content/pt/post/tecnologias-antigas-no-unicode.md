---
title: "Tecnologias Antigas no Unicode"
date: 2023-08-12T19:15:16-03:00
draft: false
description: "Como símbolos antigos afetam nossa comunicação hoje em dia"
featured_image: "/images/emojis.jpeg"
tags: ["computer-science"]
---

O [Unicode](https://home.unicode.org) é um padrão internacional para representar todos os símbolos possíveis criados pela
humanidade. Conforme o tempo vai passando, nós (humanidade) vamos criando novos símbolos, ou
reaproveitando os já existentes (🍆) com outros significados.

Aqui vou mostrar um pouco de algumas tecnologias já ultrapassadas, mas que ainda tem representação
em nossos smartphones topo de linha, graças ao Unicode.

### Fitas

Muito antes dos SDDs, costumávamos gravar dados em
fitas magnéticas enroladas dentro de cartuchos de pástico. As mais famosas eram fitas K7 e VHS:

## 📼

Hoje em dia há um movimento hipster para tentar manter vivas as fitas de músicas K7.
Empreas como a [Polysom](http://polysom.com.br) ainda publicam álbuns de alguns artistas em fitas K7.

E se você assistiu à série original de Star Trek (1966-1969), deve ter notado algumas **tapes drives**
de computadores: ✇

### Discos flexíveis

💾

Famosos nos anos 80 e 90, esses discos flexíveis eram usados para salvar dados nos computadores
pessoais. Coisa que o pendrive e a nuvem substituiram com maestria.

Esses símbolos eram usados para representar a ação de **salvar** em softwares. Conforme o
tempo foi passando, mais e mais jovens que nunca tiveram contato com discos flexíveis não
entendiam seu significado, fazendo cair em desuso.

### CD e DVD

💿 📀

Não tão antigos como as fitas magnéticas, os CDs também cairam em desuso graças a serviços de
streaming como Spotify e Netflix. Hoje em dia é até estranho entrar em alguma loja e ver fisicamente
músicas/filmes em caixinhas.

### Pagers e Máquinas de Fax

📟 📠

Dos anos 90, o [pager](https://pt.wikipedia.org/wiki/Pager) era um aparelho do tamanho de um cartão de crédito, tinha um display simples de cristal líquido,
e sua função era mostrar mensagens curtas em texto enviadas de uma central.

Nunca foi muito popular no Brasil devido ao alto custo do aparelho e mensalidades salgadas.

Já o fax era a única alternativa de envio de documentos à distância até a popularização da
internet e scanner na primeira metade dos anos 2000.

### Telefones

☏ ☎

Telefones fixos eram artigo de luxo no Brasil dos anos 80 e 90, antes da privatização das companias
telefônicas. Aparelhos de telefone com disco estão extintos, dignos de museu.

### E a história continua...

Estudar a linguagem humana é interessante, e conta muito sobre como vivemos e nos comunicamos.
Milênios depois que inventarem os algarismos romanos, nós ainda os usamos no século **XXI**, mesmo que
essa sociedade já tenha sumido da face da Terra.

Nossos **emojis** e **memes** de hoje, terão representação no futuro,
assim como [hieróglifos egípcios](https://pt.wikipedia.org/wiki/Hier%C3%B3glifos_eg%C3%ADpcios)de 5 mil anos tem representação em nossos computadores modernos.
Graças ao Unicode 🙂
