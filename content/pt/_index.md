---
title: "Blog"

description: "Brasileiro 🇧🇷 Italiano 🇮🇹, Canadense de coração 🇨🇦, 👨🏻‍💻 Engenheiro de Software e Especialista em Inteligência Artificial, morando na cidade de Sao Paulo 📌"
# 1. To ensure Netlify triggers a build on our exampleSite instance, we need to change a file in the exampleSite directory.
theme_version: "2.8.2"
cascade:
  featured_image: "/images/neural_network.jpeg"
---
Bem-vindo(a)
