---
title: "Diogo Tozzi: Blog"

description: "Brazilian🇧🇷 Italian🇮🇹, Canada Lover🇨🇦, 👨🏻‍💻 Software Engineer and Artificial Intelligence Specialist, Sao Paulo city📌"
# 1. To ensure Netlify triggers a build on our exampleSite instance, we need to change a file in the exampleSite directory.
theme_version: '2.8.2'
cascade:
  featured_image: '/images/neural_network.jpeg'
---
Welcome!
